export enum StepType {
  UPLOAD = "UPLOAD",
  NON_INTERACTIVE = "NON_INTERACTIVE",
}
