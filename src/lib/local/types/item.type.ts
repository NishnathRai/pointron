import type { DbRecordBase } from "$lib/tidy/types/dbrecord.type";
import type { UserGlobalPreferences } from "$lib/tidy/types/preferences.type";
import type { PointGoalDbType } from "./goal.type";
import type {
  FocusItemsStore,
  PointLogDbType,
  PointSessionDbType,
  SessionStore
} from "./session.type";
import type { PointTagDbType } from "./tag.type";
import type { UserLocalPreferences } from "./userLocalPreferences.type";

export type DbRecordType = DbRecordBase &
  (
    | PointGoalDbType
    | PointSessionDbType
    | PointLogDbType
    | PointTagDbType
    | SessionStore
    | UserLocalPreferences
    | UserGlobalPreferences
    | FocusItemsStore
  );
