module.exports = {
    extends: ['plugin:prettier/recommended'],
    rules: {
      'space-infix-ops': 'error', // enforce consistent spacing around infix operators
      'keyword-spacing': 'error' // enforce consistent spacing before and after keywords
    }
  };